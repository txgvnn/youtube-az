#!/usr/bin/env python
import os.path
import warnings
import sys
from distutils.core import setup

setup(
    name='youtube-az',
    version='1.0',
    description='YouTube from A to Z',
    author='TxGVNN',
    author_email='TxGVNN@gmail.com',
    scripts= ['youtube-az'],
    packages=[
        'youtube_dl',
        'youtube_dl.extractor', 'youtube_dl.downloader','youtube_dl.postprocessor',
        'youtube_upload',
        'youtube_upload.auth'
    ],
    data_files=[("share/youtube_upload", ['youtube_upload/client_secrets.json'])],
    classifiers=[
        "Topic :: Multimedia :: Video",
        "Development Status :: Production/Stable",
        "Environment :: Console",
        "License :: Public Domain",
        "Programming Language :: Python :: 2.6",
        "Programming Language :: Python :: 2.7",
    ],
)
