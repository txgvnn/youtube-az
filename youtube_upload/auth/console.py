import sys

def get_code(authorize_url):
    """Show authorization URL and return the code the user wrote."""
    message = "Check this link in your browser: \x1B[0;34m{0}\x1B[0m".format(authorize_url)
    sys.stderr.write(message + "\n")
    return raw_input("Enter verification code: ")
