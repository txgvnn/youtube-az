Introduction
============

youtube-az is command-line Python script that auto download and upload video,  title, description and tags. (it should work on any platform GNU/Linux, BSD, OS X, Windows, ... that runs Python)

Thanks for
==========
- Rg3 with  [youtube-dl](https://github.com/rg3/youtube-dl)
- Tokland with [youtube-upload](https://github.com/tokland/youtube-upload)

Dependencies
============

  * [Python 2.6 or 2.7](http://www.python.org). Python 3.0 is NOT supported.
  * Packages: [google-api-python-client](https://developers.google.com/api-client-library/python), [progressbar](https://pypi.python.org/pypi/progressbar) (optional).

Check if your operating system provides those packages. If not, you can install them with `pip`:

```sh
$ pip install --upgrade google-api-python-client progressbar
```

Install
=======
```
$git clone ...
$[sudo] python setup.py install
```

Bug fix
=================
Error Unicode:
```sh
$export LANG=en_US.utf8
```
Error SSL

On Debian/Ubuntu:
```sh
$sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm
```
