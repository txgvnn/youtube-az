#!/bin/bash
if [ "`id -u`" -ne 0 ]
then
	echo -e '\e[0;31mYou are not root\e[0m'
	exit
fi
apt-get update
apt-get install python-pip -y
pip install --upgrade google-api-python-client progressbar
echo 'export LANG=en_US.utf8' >> /etc/bash.bashrc
git clone https://txgvnn@bitbucket.org/txgvnn/youtube-az.git
cd youtube-az
python setup.py install
cd ..
rm youtube-az -Rf
echo ''
echo -e '\e[0;34mInstalled successfully\e[0m'
rm $0 -Rf
